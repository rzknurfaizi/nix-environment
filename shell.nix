with import <nixpkgs> {};

stdenv.mkDerivation {
	name = "jdk-nodejs-prices-env";
	buildInputs = [
	    # pkgs.openjdk11 # essential
	    # pkgs.nodejs # essential
	    nodePackages.json-server
	];
}
